
#!/bin/bash
set -e pipefail

# This scripts prepares (creates or updates) the .po files for each language.
# The new messages found in the php files will merged to the existing .po files.
# All untraslated messages will be attempted to be translated for using the 'transl' tool.

# Add more locale codes as needed and available in the system
ARR_LOCALES=("es_ES" "ca_ES")
POT_FILE="./locale/template/messages.pot"
LOCALES_DIR="./locale"

update_lang() {
    locale="$1"
    lang=${locale:0:2}
    echo "Update locale '$locale' with language '$lang'"

    languagePath="$LOCALES_DIR/$locale.utf8/LC_MESSAGES"
    poFile="$languagePath/messages.po"
    poTranslatedFile="$languagePath/messages-translated.po"

    if test -f "$poFile"; then
      echo "PO file '$poFile' already exists, merging new translations into existing ones ..."
      msgmerge --no-wrap --no-fuzzy-matching -U $poFile $POT_FILE
    else
      echo "PO file '$poFile' does not exists, creating a new file ..."
      msginit --no-wrap --locale=$locale --input=$POT_FILE --output-file=$poFile --no-translator;
    fi

    echo "Start translations of new messages added ..."
    python -m src.transl -i "$poFile" -o "$poTranslatedFile" -l $lang;
    mv -f $poTranslatedFile $poFile
}


if test -f "$POT_FILE"; then
    echo "Remove old $POT_FILE"
    rm -f $POT_FILE
fi
echo "Generating new template pot file '$POT_FILE' ..."
find ./public_html/ -iname '*.php' -exec xgettext --language=PHP --from-code=UTF-8 \
 --keyword=_ --no-wrap -o $POT_FILE '{}' +

for locale in "${ARR_LOCALES[@]}"
do
  echo " ---------- Update locale $locale ------------"
  update_lang $locale
done;
