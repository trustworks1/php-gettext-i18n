#!/bin/bash
set -e pipefail

# This script will recompile the .po files of each locale
# into an .mo file.

# This script has a workaround step to force the gettext tool to 
# reload the new version of the translated files. This step is 
# required when we are not able to or we do not want to restart
# server. 

LOCALES_DIR="./locale/"
CONFIG_FILE="./config/config.php"
TIMESTAMP=$(date +%s)

# Update config local version
echo "Update config locale version to $TIMESTAMP."
sed -i "s/define('LOCALE_VERSION', .*);/define('LOCALE_VERSION', $TIMESTAMP);/" $CONFIG_FILE

# Recompile the .po files into .mo files.
echo "Deletting old mo files ..."
find . -type f -name "*.mo" | xargs rm -rf

echo "Compiling new locales to version $TIMESTAMP."
find "$LOCALES_DIR" -type f -name "*.po" | while read -r pofile; do
    currentFolder=$(dirname "$pofile")
    moFileName="$currentFolder/messages_${TIMESTAMP}.mo"
    echo "Compile new $moFileName."
    msgfmt -o $moFileName $pofile
    #This step is optional. For some environments it is 
    # more stable to give some time to save files.
    sleep 1 
done
sync
echo "New locale version update done."
