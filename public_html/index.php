<?php
// TRANSLATOR required to activate the UTF-8 charset.
_("__required__ø");

require_once __DIR__ . '/../config/config.php';
require_once 'templates/localeSetup.php';
?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

   <head>
      <meta charset="UTF-8">
      <title>trustworks - i18n tool</title>
   </head>

   <body>
      <h1><?php echo _("Hello world translated"); ?></h1>
      <div>
         <p>Language menu:</p>
         <ul>
            <li>
               <a href="/?locale=en_GB.utf8" title="Switch to English Language">EN</a>
            </li>
            <li >
               <a href="/?locale=ca_ES.utf8" title="Switch to Catalan Language">CA</a>
            </li>
            <li >
               <a href="/?locale=es_ES.utf8" title="Switch to Spanish Language">ES</a>
            </li>
         </ul>
      </div>
   </body>

</html>
