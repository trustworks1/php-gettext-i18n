<?php

$supportedLocales = ['en_GB.utf8', 'ca_ES.utf8', 'es_ES.utf8'];
$defaultLocale = 'en_GB.utf8'; // Define your default locale
$cookieName = 'user_locale'; // Name of the cookie

// Check if the locale parameter is set in the query and is valid
if (isset($_GET['locale']) && in_array($_GET['locale'], $supportedLocales)) {
    $locale = $_GET['locale'];
    // Set the locale in a cookie that expires in 30 days
    setcookie($cookieName, $locale, time() + 60 * 60 * 24 * 30, '/');
    $_SESSION['user_locale'] = $locale;
} elseif (isset($_COOKIE[$cookieName]) && in_array($_COOKIE[$cookieName], $supportedLocales)) {
    // Use the locale stored in the cookie
    $locale = $_COOKIE[$cookieName];
    $_SESSION['user_locale'] = $locale;
} elseif (!isset($_SESSION['user_locale'])) {
    // Fallback to default locale
    $locale = $defaultLocale;
} else {
    // Use session locale
    $locale = $_SESSION['user_locale'];
}

// Set the locale for gettext   
putenv("LANG=" . $locale);
putenv("LANGUAGE=" . $locale);
setlocale(LC_ALL, $locale);

// To force a reload, you might append a version or timestamp
$text_domain = 'messages';
$text_domain_versioned = $text_domain . '_' . LOCALE_VERSION;
bindtextdomain($text_domain_versioned, __DIR__ . "/../../locale");
bind_textdomain_codeset($text_domain_versioned, 'utf8');
textdomain($text_domain_versioned);
