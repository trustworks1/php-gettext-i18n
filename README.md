
## Development environment setup:

> **IMPORTANT**: This project is setup into a devcontainer because the gettext tool requires that the system locales are installed 
into the OS. This devcontainer setup few locales into the [docker/Dockerfile](docker/Dockerfile) definition.

1. [Install Docker Desktop](https://docs.docker.com/desktop/)

    Note: It is possible to install only Docker Engine, Docker CLI and Docker compose. i.e on Ubuntu just run:
    ```bash
    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
    ```
    
2. Copy paste .env.dev into .env and edit the variable values as per the .env comments.

3. Open the project into a [vscode devcontainer](https://code.visualstudio.com/docs/devcontainers/containers)

    -- or --
    
    Directly start the service with docker compose.

```shell
# From project root folder
# Start
docker compose up --build --remove-orphans

# Stop
docker compose down
```


# Run scripts to create or update the translations.

```shell

pip install --upgrade pip
pyenv activate trustworks
/bin/bash -c "./scripts/update_translations.sh"
/bin/bash -c "./scripts/update_locale_version.sh"
```

# Test the translated content

Open the test website at http://localhost:8081

> Click the Menu language menu links to see if the translated text is displayed correctly.

# Run python tests

```shell
# From the project root on a devcontainer shell just run:
pytest
```
