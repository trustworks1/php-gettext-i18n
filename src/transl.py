import argparse

from src.__init__ import logger
from src.infrastructure.fs_client import load_file_into_list_strings, write_output_file, verify_arg_file_exists
from src.domain.translator_service import Translator_Service
from src.infrastructure.translator_EN import Translator_EN


class TranslatorPHP:
    
    translator_service: Translator_Service = Translator_Service()

    def __init__(self):
        pass

    def main(self, in_args) -> None:
        logger.debug('---- Start -----')
        input_po_file = in_args.input_po_file
        output_po_file = in_args.output_po_file
        language = in_args.language
        logger.info(f"\n  Tranlate file '{input_po_file}' to lang '{language}', "+
                    f"\n  into output file {output_po_file}")
        
        input_po_lines = load_file_into_list_strings(input_po_file)        
        output_po_lines = self.translator_service.translate(input_po_lines, language)
                
        write_output_file(content=output_po_lines, output_file_path=output_po_file)    
     

class ListLangsAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        parser.print_usage()
        langs_dict = Translator_EN.get_supported_langs()
        print(f"Use the target language name or code from the list:")
        langs = [f"    Name: {key}, Code: {value}" for key, value in langs_dict.items()]
        print('\n'.join(langs))
        parser.exit()
         

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Translate po files msgstr values, from English to the given language.")
    parser.add_argument("-i", "--input_po_file", 
        help="Input file to be translated.",
        required=True,
        type=verify_arg_file_exists)
    parser.add_argument("-o", "--output_po_file",
        help="Translated po file.",
        required=True)
    parser.add_argument("-l", "--language",
        help="Target translation language. Run --help to see the list of available languages.",
        required=True)
    parser.add_argument("--list-languages", nargs=0, action=ListLangsAction, help="Show list of available target translation languages.")
    TranslatorPHP().main(parser.parse_args())
