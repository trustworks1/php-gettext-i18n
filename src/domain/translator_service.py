from src import logger
from io import StringIO
from typing import List
from src.domain.messages_service import Messages_service
from src.infrastructure.translator_EN import Translator_EN

class Translator_Service:
    
    msgs_srv = Messages_service()    

    def is_translatable(self, text):
        if text != "":
            head = text[:3]
            return any(char.isalnum() for char in head)
        else:
            return False  
   
    def translate(self, po_lines: List[str], language: str):
        output_lines = StringIO()
        translator_en_to_es = Translator_EN(language)
        i = 0
        while i < len(po_lines):
            line = po_lines[i].strip()
            
            if self.msgs_srv.is_msgid(line):
                msgid_content = self.msgs_srv.read_msgid_content(line)                
                output_lines.write(f"{line}\n")
                
                if self.msgs_srv.is_msgid_single_line(msgid_content, po_lines, i):
                    if self.msgs_srv.is_single_line_translated(po_lines, i):
                        i += 1
                        msgstr_line = po_lines[i].strip()
                        output_lines.write(f"{msgstr_line}\n")
                    else:
                        if self.is_translatable(msgid_content):
                            logger.info(f"Translate line {i} with msgid '{msgid_content[:20]}'")
                            msgstr_result = translator_en_to_es.translate(msgid_content)
                            output_lines.write(f'msgstr "{msgstr_result}"\n')
                        else:
                            output_lines.write(f'msgstr ""\n')
                        i += 1
                else:
                    #Multiline case
                    msgid_original_pharagraph, msgid_ph_str, i = \
                        self.msgs_srv.read_msgid_pharagraph(po_lines, i)
                    
                    if msgid_original_pharagraph != "":
                        self.write_pharagraph_to(msgid_original_pharagraph, output_lines)
                    
                    if i<len(po_lines):
                        msgstr_value, msgstr_result = "", ""
                        msgstr_line = po_lines[i].strip()
                        if "msgstr" not in msgstr_line:
                            logger.warn(f"msgstr missing for msgid at line {i}")
                        logger.debug(f"Read msgstr at line {i}, '{msgstr_line}'")
                        msgstr_content = self.msgs_srv.read_msgstr_content(msgstr_line)                    
                        if msgstr_content != "":
                            msgstr_value = msgstr_content                     
                        else:                    
                            if i+1 < len(po_lines):
                                msgstr_line_value = po_lines[i+1].strip()
                                msgstr_value = self.msgs_srv.read_line_content(msgstr_line_value)
                            
                        if msgstr_value == "":
                            if self.is_translatable(msgid_ph_str):
                                logger.info(f"Translate line {i} with pharagraph msgid '{msgid_ph_str[:20]}...'")
                                msgstr_result = translator_en_to_es.translate(msgid_ph_str)
                        else:
                            i += 1
                            msgstr_result = msgstr_value
                                                
                        output_lines.write('msgstr ""\n')
                        output_lines.write(f'"{msgstr_result}"\n')
                    else:
                        logger.warn(f"msgstr missing for msgid at line {i}")
                        
            else:
                output_lines.write(f"{line}\n")
            i += 1
            
        translated_content = output_lines.getvalue()
        output_lines.close()
            
        return translated_content
    
    
    def write_pharagraph_to(self, pharagraph_lines, file_output):
        for line in pharagraph_lines:    
            file_output.write(f'"{line}"\n') 
