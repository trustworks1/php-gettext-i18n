import re

class Msg_Formatter:

    def trim_extra_spaces(self, text, min=1):
        rexp = "[^\\S\n]{"+str(min)+",}"
        pattern = re.compile(rexp)
        return re.sub(pattern, ' ', text)
    
    def remove_new_line(self, text):
        return re.sub(r'[\n]', '', text)
    
    def remove_quotes_backslash(self, text):
        return text.replace('\\"', '"')
    
    def add_quotes_backslash(self, msgstr_value):
        return msgstr_value.replace('"', '\\"')
