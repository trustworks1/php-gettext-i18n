import re

class Messages_service:
    
    def is_msgid(self, line):
        return line.startswith('msgid')
    
    def read_msgid_content(self, line):
        lst = re.findall(r'msgid "(.*)"', line)
        if len(lst) == 0:
            return ""
        else:
            return lst[0]
    
    def read_msgstr_content(self, line):
        lst = re.findall(r'msgstr "(.*)"', line)
        if len(lst) == 0:
            return ""
        else:
            return lst[0] 
    
       
    def is_single_line_translated(self, lines, i):
        j = i+1
        if j <= len(lines):
            next_line = lines[j].strip()
            return 'msgstr' in next_line and self.read_msgstr_content(next_line) != ""
        else:
            raise Exception(f'Missing msgstr for msgid at line {i}')
    
    def is_msgid_single_line(self, msgid_content, lines, i):
        j = i+1
        if j <= len(lines):
            return msgid_content != "" or "msgstr" in lines[i+1]        
        else:
            raise Exception(f'Missing msgstr for msgid at line {i}')
        
    def read_msgid_pharagraph(self, lines, i):
        i += 1
        msgid_original_pharagraph = []    
        while self.is_multi_line_msgid(lines, i):
            msgid_ph_line = self.read_line_content(lines[i])
            msgid_original_pharagraph.append(msgid_ph_line)
            i += 1
        msgid_pharagraph_str = ' '.join(msgid_original_pharagraph)
        return msgid_original_pharagraph, msgid_pharagraph_str, i
    
    def is_multi_line_msgid(self, lines, i):
        return i < len(lines) and lines[i].startswith('"')
    
    def read_line_content(self, line):
        lst = re.findall(r'"(.*)"', line)
        if len(lst) == 0:
            return ""
        else:
            return lst[0]
    

        