
from src import logger
from src.domain.formatter import Msg_Formatter
from deep_translator import GoogleTranslator

class Translator_EN:
    msg_formatter = Msg_Formatter()
    
    def __init__(self, language: str):
        self.google_translator = GoogleTranslator(source='en', target=language)   
    
    def translate(self, msg_content):       
        trimmed_text = self.msg_formatter.trim_extra_spaces(msg_content)
        
        if trimmed_text == "":
            return ""
        else:
            no_new_line_text = self.msg_formatter.remove_new_line(trimmed_text)
            no_backslash_text = self.msg_formatter.remove_quotes_backslash(no_new_line_text)
            translated_text = self._translate_text(no_backslash_text)
            logger.debug(f"Translated to: '{translated_text[:20]}'")
            return self.msg_formatter.add_quotes_backslash(translated_text)
        
    @staticmethod
    def get_supported_langs() -> dict:
        return GoogleTranslator().get_supported_languages(as_dict=True)

    def _translate_text(self, text):        
        logger.debug(f"Translatting text '{text[:20]}...'")
        try:
            return self.google_translator.translate(text)
        except Exception as e:
            logger.error(f"An error occurred: {e}")
            raise Exception(f"Error while trying to translate text '{text[:20]}...': {e}")
        
    
        