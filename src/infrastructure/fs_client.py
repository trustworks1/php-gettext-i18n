import os

from io import StringIO
from src import logger
from argparse import ArgumentTypeError

def verify_arg_file_exists(file_path):
    if not os.path.isfile(file_path):
        raise ArgumentTypeError(f"{file_path} does not exist.")
    return file_path

def load_file_into_list_strings(input_file_path):
    with open(input_file_path, 'r', encoding='utf-8') as f:
        logger.debug(f"File {input_file_path} loaded.")
        return f.readlines()
    
def write_output_file(content :str, output_file_path):
    with open(output_file_path, 'w', encoding='utf-8') as f:
        f.write(content)
        logger.debug(f"Result saved to file '{output_file_path}'.")
