import pytest

from src import logger

@pytest.mark.parametrize("sample_text, expected", 
                         [("", ""),
                          ("Services", "Servicios"),
                          (r'In \"regulation\", \"directive\",', 
                           r'En \"reglamento\", \"directiva\",')])
def test_translate_text_to_given_lang(sample_text, expected, translator_en_to_es):
    logger.debug(f"Translate text '{sample_text}'")
    result = translator_en_to_es.translate(sample_text)
    logger.debug(f"Result '{result}'")
    assert result == expected
    
