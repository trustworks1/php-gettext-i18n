import logging

from src.infrastructure.fs_client import load_file_into_list_strings, write_output_file

logger = logging.getLogger(__name__)

def test_should_load_input_file(empty_msgid_path):
    lines = load_file_into_list_strings(empty_msgid_path)
    assert lines != None
    assert len(lines) > 1
    
def test_should_write_output_file(temp_file_path):
    sample_content = "Translated file is cool"
    write_output_file(content=sample_content, output_file_path=temp_file_path)
    with open(temp_file_path, 'r', encoding='utf-8') as f:
        saved_lines=f.readlines()
    assert sample_content in saved_lines
    