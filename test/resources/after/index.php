<?php
// TRANSLATOR required to activate the UTF-8 charset.
_("__required__ø");


?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head></head>
   

<body class="resource-template-default single single-resource postid-13201  lang-en hrr">
   <!-- End Google Tag Manager (noscript) -->
   <header class="navbar navbar-expand-xl trustworks-navbar">
      <nav class="container-xl" aria-label="Main navigation">
         <a class="navbar-brand" href="/" title="Back to Homepage">
            <img src="assets/images/img_trustworks_logo.svg" alt="trustworks Company Logo">
         </a>
         <button class="navbar-toggler" type="button" aria-label="Toggle navigation">
            <svg class="icon-hamburger" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="#fff"
               viewBox="0 0 16 16">
               <path fill-rule="evenodd"
                  d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z">
               </path>
            </svg>
            <svg class="icon-close" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.63 15.62">
               <polygon fill="#fff"
                  points="15.63 1.41 14.21 0 7.81 6.4 1.41 0 0 1.41 6.4 7.81 0 14.21 1.41 15.63 7.81 9.23 14.21 15.63 15.63 14.21 9.23 7.81 15.63 1.41" />
            </svg>
         </button>
         <div class="collapse navbar-collapse" id="navbar">
            <div class="menu-header-menu-navigation-container">
               <ul id="trustworks-header-menu" class="menu ">
                  <li
                     class="menu-item menu-item-type-custom menu-item-object-custom nav-item menu-item-has-children dropdown">
                     <a href="/services" class="nav-link dropdown-toggle" title="Explore our range of services">
                        <?php echo _("Services"); ?>
                     </a>
                  </li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom nav-item">
                     <a href="/methodology" class="nav-link" title="Learn about Our Methodology">
                        <?php echo _("Methodology"); ?>
                     </a>
                  </li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom nav-item">
                     <a href="/partners" class="nav-link" title="Explore Our Partners">
                        <?php echo _("Partners"); ?>
                     </a>
                  </li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom nav-item">
                     <a href="/insights" class="nav-link" title="Gain Insights and Knowledge">
                        <?php echo _("Insights"); ?>
                     </a>
                  </li>
                  <!-- <li class="menu-item menu-item-type-custom menu-item-object-custom nav-item">
                        <a href="/about" class="nav-link" title="About Us"><?php echo _("About"); ?></a>
                     </li> -->
                  <li class="menu-item menu-item-type-custom menu-item-object-custom nav-item">
                     <a href="/contact" class="nav-link" title="Contact Us">
                        <?php echo _("Contact"); ?>
                     </a>
                  </li>
               </ul>
               <ul class="navbar-nav trustworks-navbar-right flex-row ms-md-auto">
                  <li class="menu-item demos-dropdown-wrapper">
                     <div class="nav-link btn btn-primary product-demos-dropdown"
                        title="Schedule a Meeting with trustworks" data-cal-link="trustworks/contact"
                        data-cal-namespace="contact" data-cal-config='{"layout":"month_view"}'>
                        <?php echo _("Request a meeting"); ?>
                     </div>

                  </li>
                  <li
                     class="menu-item menu-item-type-custom menu-item-object-custom dropdown nav-item languages-dropdown">
                     <a href="javascript:void(0)" class="nav-link dropdown-toggle" title="Select Your Language">
                        <svg width="33" height="18" enable-background="new 0 0 33.2 18.2" viewBox="0 0 33.2 18.2"
                           xmlns="http://www.w3.org/2000/svg">
                           <title>
                              <?php echo _("Languages"); ?>
                           </title>
                           <path d="m32.4 7.4-3.5 3.5-3.5-3.5" fill="none" stroke="currentColor" stroke-width="2" />
                           <path fill="currentColor"
                              d="m9.2.1c-5 0-9 4-9 9s4 9 9 9 9-4 9-9-4-9-9-9zm0 16.6c-4.2 0-7.6-3.4-7.6-7.6s3.4-7.6 7.6-7.6h.7v1.3h-1.4l-.7 2.2h-.7v.7h3.5l.7.7h.7v.6l-.7.7-2.8-.7-2.8 2.1v2.1l1.4 1.4h2.1v1.7l.7 1.7h.7l2.1-2.8v-.7l.7-1.4v-.7h-.7l-.7-1.3.7-.7 1 .7 1.7-.7v-.7h1.3c.1.5.1.9.1 1.4 0 4.2-3.4 7.6-7.6 7.6z" />
                        </svg>
                     </a>
                     <ul class="dropdown-menu depth_0">                        
                        <?php include PUBLIC_PATH.'assets/menu/langs.php';?>
                     </ul>
                  </li>
               </ul>
               <ul class="menu languages-mobile">
                  <li class="megamenu menu-item menu-item-type-custom menu-item-object-custom dropdown nav-item">
                     <a href="javascript:void(0)" class="nav-link dropdown-toggle" title="Open Dropdown Menu">
                        <svg width="33" height="18" viewBox="0 0 33 18" version="1.1" xmlns="http://www.w3.org/2000/svg"
                           xmlns:xlink="http://www.w3.org/1999/xlink">
                           <title>
                              <?php echo _("Languages"); ?>
                           </title>
                           <path fill="currentColor"
                              d="M9,0 C4.03753846,0 0,4.03753846 0,9 C0,13.9624615 4.03753846,18 9,18 C13.9624615,18 18,13.9624615 18,9 C18,4.03753846 13.9624615,0 9,0 Z M9,16.6153846 C4.80115385,16.6153846 1.38461538,13.1988462 1.38461538,9 C1.38461538,4.80115385 4.80115385,1.38461538 9,1.38461538 C9.234,1.38461538 9.46384615,1.39915385 9.69230769,1.41992308 L9.69230769,2.76923077 L8.30769231,2.76923077 L7.61538462,4.84615385 L6.92307692,4.84615385 L6.92307692,5.53846154 L10.3846154,5.53846154 L11.0769231,6.23076923 L11.7692308,6.23076923 L11.7692308,6.92307692 L11.0769231,7.61538462 L8.30769231,6.92307692 L5.53846154,9 L5.53846154,11.0769231 L6.92307692,12.4615385 L9,12.4615385 L9,14.1923077 L9.69230769,15.9230769 L10.3846154,15.9230769 L12.4615385,13.1538462 L12.4615385,12.4615385 L13.1538462,11.0769231 L13.1538462,10.3846154 L12.4615385,10.3846154 L11.7692308,9 L12.4615385,8.30769231 L13.5,9 L15.2307692,8.30769231 L15.2307692,7.61538462 L16.4831538,7.61538462 C16.5662308,8.06538462 16.6153846,8.52646154 16.6153846,9 C16.6153846,13.1988462 13.1988462,16.6153846 9,16.6153846 Z">
                           </path>
                        </svg>
                     </a>
                     <ul class="dropdown-menu depth_0">                        
                        <?php include PUBLIC_PATH.'assets/menu/langs.php';?>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
   </header>
   <section class="block-hero-full-image">
      <div class="container-xl">
         <div class="row">
            <div class="col-12 col-md-8">
               <div class="hero-content">
                  <h1 class="lrg">
                     <?php echo _("Security, Quality and Regulatory"); ?>
                  </h1>
                  <p>
                     <?php echo _("Enhancing Information Security, Elevating Quality Assistance and Ensuring Regulatory Compliance through our Trust Systems Integration Methodology."); ?>
                  </p>
                  <a class="btn btn-primary btn-lg" href="methodology" target="_self"
                     title="Learn more about our methodology">
                     <span>
                        <?php echo _("Discover our Methodology"); ?>
                     </span>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <img decoding="async" class="hero-bg-img" loading="eager" fetchpriority="high"
         src="assets/images/img_homepage_banner.svg" alt="Background gradient" width="2560" height="1200">
   </section>
   <div class="in-page-nav-wrapper">
      <nav class="in-page-nav mb-0">
         <a class="js-in-page-link" href="#test-automation" target="_self" title="Navigate to Test Automation section">
            <?php echo _("Test Automation"); ?>
         </a>
         <a class="js-in-page-link" href="#enterprise-devsecops" target="_self"
            title="Navigate to Enterprise DevSecOps section">
            <?php echo _("Enterprise DevSecOps"); ?>
         </a>
         <a class="js-in-page-link" href="#information-security" target="_self"
            title="Navigate to Information Security section">
            <?php echo _("Information Security"); ?>
         </a>
         <a class="js-in-page-link" href="#quality-assistance" target="_self"
            title="Navigate to Quality Assistance section">
            <?php echo _("Quality Assistance"); ?>
         </a>
         <a class="js-in-page-link" href="#regulatory-compliance" target="_self"
            title="Navigate to Regulatory Compliance section">
            <?php echo _("Regulatory Compliance"); ?>
         </a>
      </nav>
   </div>
   <section class="block-content data-cards" id="test-automation">
      <div class="container-xl">
         <div class="row mb-6">
            <div class="col-12 col-md-5">
               <h2 class="title-md mb-3">
                  <?php echo _("Overlooking Test Automation"); ?>
               </h2>
            </div>
            <div class="col-12 col-md-7 wyswyg-content wyswyg-large aos-init  ">
               <p>
                  <?php echo _("In today's agile and CI/CD landscape, bypassing test automation directly correlates to inflated operational
                     costs and heightened error risks. As software complexity scales, the absence of automation transitions from
                     an option to a fiduciary liability. In such a scenario, failing to automate puts companies at financial risks."); ?>
               </p>
            </div>
         </div>
         <div class="row">
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color: white">
                        <?php echo _("Up to 30% longer software development cycles can occur when test automation is skipped."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_automation_block_1.svg" loading="lazy"
                        alt="Test Automation Impact Statistics">
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color: white">
                        <?php echo _("Manual testing struggles to keep up with frequent code changes, reducing efficiency by 25%."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_automation_block_2.svg" loading="lazy"
                        alt="Manual Testing Efficiency Statistics">
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color: white;">
                        <?php echo _("Without test automation, scaling becomes a hard process, increasing operational costs by 40%."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_automation_block_3.svg" loading="lazy"
                        alt="Operational Costs Statistics">
                  </div>
               </div>
            </div>
         </div>
         <div class="cards-bottom-button">
            <a class="btn btn-secondary btn-lg" href="/services/test-automation" target="_self"
               title="Learn more about Cut Costs, Cut Risks: Automate Your Tests"><span>
                  <?php echo _("Cut Costs, Cut Risks: Automate Your Tests</span>"); ?></a>
         </div>
      </div>
   </section>
   <section class="block-content data-cards light-bg" id="enterprise-devsecops">
      <div class="container-xl">
         <div class="row mb-6">
            <div class="col-12 col-md-5">
               <h2 class="title-md mb-3">
                  <?php echo _("Critical DevSecOps testing gap"); ?>
               </h2>
            </div>
            <div class="col-12 col-md-7 wyswyg-content wyswyg-large aos-init  ">
               <p>
                  <?php echo _("Scrutinize the tangible advantages and critical importance of integrating testing into your DevSecOps pipeline. With the acceleration of software development cycles, failing to incorporate automated testing steps it's a significant risk. Failing to do so can compromise the overall efficiency of your development and operational processes."); ?>
               </p>
            </div>
         </div>
         <div class="row">
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color: white;">
                        <?php echo _("Bypassing automated testing in a DevSecOps pipeline can lengthen the SDLC by up to 25%."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_devops_block_1.svg" loading="lazy"
                        alt="DevSecOps Pipeline Statistics"">
                     </div>
                  </div>
               </div>
               <div class=" col-12 col-md-4 data-card-col">
                     <div class="data-card">
                        <div class="data-card-inner">
                           <p style="color: white;">
                              <?php echo _("Without integrated testing, manual error rates in CI/CD processes can escalate by approximately 30%."); ?>
                           </p>
                           <img width="567" height="368" decoding="async" class="data-01"
                              src="assets/images/img_homepage_devops_block_2.svg" loading="lazy"
                              alt="CI/CD Process Error Rates">
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-4 data-card-col">
                     <div class="data-card">
                        <div class="data-card-inner">
                           <p style="color: white;">
                              <?php echo _("Overlooking automated testing restricts continuous feedback, decreasing efficiency by 20%."); ?>
                           </p>
                           <img width="567" height="368" decoding="async" class="data-01"
                              src="assets/images/img_homepage_devops_block_3.svg" loading="lazy"
                              alt="Efficiency Decrease Statistics">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cards-bottom-button">
                  <a class="btn btn-secondary btn-lg" href="/services/enterprise-devsecops" target="_self"
                     title="Explore Close the DevSecOps Testing Gap: Learn the Benefits"><span>
                        <?php echo _("Close the DevSecOps Testing Gap: Learn the Benefits"); ?>
                     </span></a>
               </div>
            </div>
   </section>

   <section class="block-content data-cards" id="information-security">
      <div class="container-xl">
         <div class="row mb-6">
            <div class="col-12 col-md-5">
               <h2 class="title-md mb-3">
                  <?php echo _("The Imperative of Security"); ?>
               </h2>
            </div>
            <div class="col-12 col-md-7 wyswyg-content wyswyg-large aos-init  ">
               <p>
                  <?php echo _("Decipher the escalating threat landscape and recognize the indispensable role of proactive cybersecurity measures in safeguarding your assets. In an era that's increasingly defined by sweeping digital transformation, the lack of a comprehensive and robust cybersecurity it's akin to a ticking time bomb."); ?>
               </p>
            </div>
         </div>
         <div class="row">
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color:white;">
                        <?php echo _("80% of organizations experienced a cyberattack in the past 3 years, signaling growing threats."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_security_block_1.svg" loading="lazy"
                        alt="Cyberattack Statistics">
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color:white;">
                        <?php echo _("90% of businesses report financial loss due to cyber incidents, underlining the economic impact."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_security_block_2.svg" loading="lazy"
                        alt="Financial Loss Due to Cyber Incidents">
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color:white;">
                        <?php echo _("70% of companies don’t expect the situation to ease in the next year, indicating persistent challenges."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_security_block_3.svg" loading="lazy"
                        alt="Continued Cybersecurity Challenges">
                  </div>
               </div>
            </div>
         </div>
         <div class="cards-bottom-button">
            <a class="btn btn-secondary btn-lg" href="/services/information-security" target="_self"
               title="Discover how to Avoid the Inevitable: Be Proactive in Cybersecurity"><span>
                  <?php echo _("Avoid the Inevitable: Be Proactive in Cybersecurity"); ?>
               </span></a>
         </div>
      </div>
   </section>
   <section class="block-content data-cards light-bg" id="quality-assistance">
      <div class="container-xl">
         <div class="row mb-6">
            <div class="col-12 col-md-5">
               <h2 class="title-md mb-3">
                  <?php echo _("Quality Assistance<br>transition"); ?>
               </h2>
            </div>
            <div class="col-12 col-md-7 wyswyg-content wyswyg-large aos-init  ">
               <p>
                  <?php echo _("Explore the shift from traditional Quality Assurance to Quality Assistance, an innovative approach that integrates quality requirements seamlessly into every step of the development lifecycle. In today's fast-paced agile environments, where adaptability and speed are key, Quality Assistance is an absolute necessity."); ?>
               </p>
            </div>
         </div>
         <div class="row">
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color: white;">
                        <?php echo _("55% of teams utilizing Quality Assistance in their workflow report fewer software defects."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_qa_block_1.svg" loading="lazy" alt="Software Defects Reduction">
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color: white;">
                        <?php echo _("40% faster feedback loops achieved with integrated Quality Assistance."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_qa_block_2.svg" loading="lazy" alt="Feedback Loop Efficiency">
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color: white;">
                        <?php echo _("65% of firms report better collaboration, satisfaction, and retention."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_qa_block_3.svg" loading="lazy"
                        alt="Collaboration, Satisfaction, Retention Improvement">
                  </div>
               </div>
            </div>
         </div>
         <div class="cards-bottom-button">
            <a class="btn btn-secondary btn-lg" href="/services/quality-assistance" target="_self"
               title="Find out about Discover the Benefits of Quality Assistance"><span>
                  <?php echo _("Discover the Benefits of Quality Assistance</span>"); ?></a>
         </div>
      </div>
   </section>
   <section class="block-content data-cards" id="regulatory-compliance">
      <div class="container-xl">
         <div class="row mb-6">
            <div class="col-12 col-md-5">
               <h2 class="title-md mb-3">
                  <?php echo _("Industry standards for operations"); ?>
               </h2>
            </div>
            <div class="col-12 col-md-7 wyswyg-content wyswyg-large aos-init  ">
               <p>
                  <?php echo _("Unpack the mounting pressures and tangible benefits of adhering to industry regulations and securing the necessary certifications for your operations. In a world that is becoming more and more regulated by the day, the failure to comply with these standards is a direct route leading to complicated legal issues and potential reputational damage."); ?>
               </p>
            </div>
         </div>
         <div class="row">
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color: white;">
                        <?php echo _("73% of businesses failing to comply now immediately face severe financial penalties."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_rc_block_1.svg" loading="lazy"
                        alt="Financial Penalties for Non-Compliance">
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color: white;">
                        <?php echo _("88% of certified firms actively report enhanced better readiness for new opportunities."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_rc_block_2.svg" loading="lazy"
                        alt="Enhanced Readiness for Opportunities">
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-4 data-card-col">
               <div class="data-card">
                  <div class="data-card-inner">
                     <p style="color: white;">
                        <?php echo _("Only 40% of surveyed companies are fully prepared for all upcoming industry regulations."); ?>
                     </p>
                     <img width="567" height="368" decoding="async" class="data-01"
                        src="assets/images/img_homepage_rc_block_3.svg" loading="lazy"
                        alt="Preparedness for Industry Regulations">
                  </div>
               </div>
            </div>
         </div>
         <div class="cards-bottom-button">
            <a class="btn btn-secondary btn-lg" href="/services/regulatory-compliance" target="_self"
               title="Immediate Action Required: Navigate Regulatory Risks Now"><span>
                  <?php echo _("Immediate Action Required: Navigate Regulatory Risks Now"); ?>
                  <?php echo _("This is a new sample text to be translated"); ?>
               </span></a>
         </div>
      </div>
   </section>
   <section class="block-content block-form-revamped bg-green" id="report">
      <div class="container-xl">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-7 col-xl-6">
               <div class="inner-content">
                  <h3 class="subtitle" style="color: white !important;">
                     <?php echo _("CONTACT"); ?>
                  </h3>
                  <h2>
                     <?php echo _("Interested?"); ?><br>
                     <?php echo _("Reach out to us"); ?>
                  </h2>
                  <p class="intro">
                     <?php echo _("Let's discuss how our services can support your business goals. Feel free to ask us any questions."); ?>
                  </p>
                  <div class="trustworks-forms blank">
                     <div class="forms-inner">
                        <form id="hs-form-private" method="POST" action="contact-form.php"
                           class="hs-form-private hs-form stacked">
                           <input type="hidden" name="csrf_token" value="<?php echo $csrf_token; ?>">
                           <div class="hs_email hs-email hs-fieldtype-text field hs-form-field">
                              <label class="" placeholder="Enter your Email" for="email"><span>
                                    <?php echo _("Email"); ?>
                                 </span><span class="hs-form-required">
                                    <?php echo _("*"); ?>
                                 </span></label>
                              <div class="input"><input id="email" name="email" required placeholder="" type="email"
                                    class="hs-input" inputmode="email" autocomplete="off" value="" maxlength="255">
                              </div>
                              <label id="emailError" class="hs-error-msg" style="display: none;"></label>
                           </div>
                           <div class="hs_firstname hs-firstname hs-fieldtype-text field hs-form-field">
                              <label class="" placeholder="Enter your First name" for="firstname"><span>
                                    <?php echo _("First name"); ?>
                                 </span><span class="hs-form-required">
                                    <?php echo _("*"); ?>
                                 </span></label>
                              <div class="input"><input id="firstname" name="firstname" required placeholder=""
                                    type="text" class="hs-input" inputmode="text" autocomplete="off" value=""
                                    maxlength="255"></div>
                              <label id="firstnameError" class="hs-error-msg" style="display: none;"></label>
                           </div>
                           <div class="hs_lastname hs-lastname hs-fieldtype-text field hs-form-field">
                              <label class="" placeholder="Enter your Last name" for="lastname"><span>
                                    <?php echo _("Last name"); ?>
                                 </span><span class="hs-form-required">
                                    <?php echo _("*"); ?>
                                 </span></label>
                              <div class="input"><input id="lastname" name="lastname" required placeholder=""
                                    type="text" class="hs-input" inputmode="text" autocomplete="off" value=""
                                    maxlength="255"></div>
                              <label id="lastnameError" class="hs-error-msg" style="display: none;"></label>
                           </div>
                           <div class="hs_organization hs-organization hs-fieldtype-text field hs-form-field">
                              <label id="label-organization" class="" placeholder="Enter your Organization"
                                 for="organization"><span>
                                    <?php echo _("Organization"); ?>
                                 </span><span class="hs-form-required">
                                    <?php echo _("*"); ?>
                                 </span></label>
                              <div class="input"><input id="organization" name="organization" required placeholder=""
                                    type="text" class="hs-input" inputmode="text" autocomplete="off" value=""
                                    maxlength="50"></div>
                              <label id="organizationError" class="hs-error-msg" style="display: none;"></label>
                           </div>
                           <div class="hs_jobtitle hs-jobtitle hs-fieldtype-text field hs-form-field">
                              <label id="label-jobtitle" class="" placeholder="Enter your Job title"
                                 for="jobtitle"><span>
                                    <?php echo _("Job title"); ?>
                                 </span><span class="hs-form-required">
                                    <?php echo _("*"); ?>
                                 </span></label>
                              <div class="input"><input id="jobtitle" name="jobtitle" required placeholder=""
                                    type="text" class="hs-input" inputmode="text" autocomplete="off" value=""
                                    maxlength="255"></div>
                              <label id="jobtitleError" class="hs-error-msg" style="display: none;"></label>
                           </div>
                           <div class="hs_phone hs-phone hs-fieldtype-phonenumber field hs-form-field">
                              <label id="label-phone" class="" placeholder="Enter your Phone number" for="phone"><span>
                                    <?php echo _("Phone number"); ?>
                                 </span></label>
                              <div class="input"><input id="phone" name="phone" placeholder="" type="tel"
                                    class="hs-input" inputmode="tel" autocomplete="off" value="" maxlength="50"></div>
                              <label id="phoneError" class="hs-error-msg" style="display: none;"></label>
                           </div>
                           <div class="hs_message hs-message hs-fieldtype-text field hs-form-field">
                              <label id="label-message" class="" placeholder="How can we help?" for="message"><span>
                                    <?php echo _("How can we help?"); ?>
                                 </span><span class="hs-form-required">
                                    <?php echo _("*"); ?>
                                 </span></label>
                              <div class="input"><textarea id="message" name="message" required placeholder=""
                                    type="text" class="hs-input" inputmode="text" autocomplete="off" value=""
                                    maxlength="500"></textarea></div>
                              <label id="messageError" class="hs-error-msg" style="display: none;"></label>
                           </div>
                           <div class="legal-consent-container">
                              <div>
                                 <div class="hs-dependent-field">
                                    <div class="field hs-form-field label-active">
                                       <div class="input">
                                          <ul class="inputs-list" required>
                                             <li class="hs-form-booleancheckbox">
                                                <label class="hs-form-booleancheckbox-display">
                                                   <div class="input"><input required class="hs-input" type="checkbox"
                                                         id="concent" name="concent" value="true"></div>
                                                   <span>
                                                      <p><span class="hs-form-required"></span>
                                                         <?php echo _("By consenting to receive electronic messages and submitting this form, I understand that my data will be handled as outlined in our "); ?><a
                                                            href="/privacy-policy" target="_blank"
                                                            title="Read our Privacy Policy"
                                                            style="text-decoration: none"><b>
                                                               <?php echo _("Privacy Policy"); ?>
                                                            </b></a>. *
                                                      </p>
                                                   </span>
                                                   <label id="concentError" class="hs-error-msg"
                                                      style="display: none;"></label>
                                                </label>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="hs_submit hs-submit">
                              <div class="actions"><input type="submit" class="hs-button primary large" value="Submit">
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5 col-xl-6 col-stick-right">
               <div class="form-revamped-img">
                  <img width="1000" height="1000" decoding="async" class="data-01"
                     src="assets/images/img_homepage_report.svg" loading="lazy" alt="Contact Form Illustration">
               </div>
            </div>
         </div>
      </div>
   </section>
   <footer class="block-content">
      <div class="container-xl">
         <div class="row">
            <div class="col-12 col-md-4 mb-5 mb-md-0">
               <a class="navbar-brand" href="/" title="Back to Homepage">
                  <img width="171" height="40" src="assets/images/img_trustworks_logo.svg" alt="trustworks Company Logo"
                     loading="lazy" class="mb-1 mb-md-1" />
               </a><p></p>
               <ul class="social d-flex justify-content-start">
                  <li><a href="https://www.linkedin.com/company/trustworksch/" target="_blank" rel="noopener"
                        title="Follow us on LinkedIn">
                        <img width="18" height="18" loading="lazy" src="assets/images/img_in.svg" alt="LinkedIn" />
                     </a>
                  </li>
                  <li><a href="https://twitter.com/trustworks_" target="_blank" rel="noopener"
                        title="Follow us on Twitter">
                        <img width="18" height="18" loading="lazy" src="assets/images/img_twitter.svg" alt="Twitter" />
                     </a>
                  </li>
               </ul>
            </div>
            <div class="col-12 col-sm-8">
               <div class="row">
                  <div class="col-6 col-sm-6 col-md-3 mb-4 mb-md-0">
                     <div class="menu-footer-menu-col-1-product-en-container">
                     </div>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3 mb-4 mb-md-0">
                     <div class="menu-footer-menu-col-2-solution-en-container">
                        <ul id="trustworks-footer-menu-col-2" class="menu">
                           <li id="menu-item-9921"
                              class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9921">
                              <a href="/services" title="Explore our Services">
                                 <?php echo _("SERVICES"); ?>
                              </a>
                              <ul class="sub-menu">
                                 <li id="menu-item-9922"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9922">
                                    <a href="services/test-automation" class="dropdown-item"
                                       title="Learn about Test Automation">
                                       <?php echo _("Test Automation"); ?>
                                    </a>
                                 </li>
                                 <li id="menu-item-9923"
                                    class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-9923">
                                    <a href="services/enterprise-devsecops" class="dropdown-item"
                                       title="Discover Enterprise DevSecOps">
                                       <?php echo _("Enterprise DevSecOps"); ?>
                                    </a>
                                 </li>
                                 <li id="menu-item-15158"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15158">
                                    <a href="services/information-security" class="dropdown-item"
                                       title="Information Security Services">
                                       <?php echo _("Information Security"); ?>
                                    </a>
                                 </li>
                                 <li id="menu-item-13055"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13055">
                                    <a href="services/quality-assistance" class="dropdown-item"
                                       title="Quality Assistance Solutions">
                                       <?php echo _("Quality Assistance"); ?>
                                    </a>
                                 </li>
                                 <li id="menu-item-13055"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13055">
                                    <a href="services/regulatory-compliance" class="dropdown-item"
                                       title="Regulatory Compliance Guidance">
                                       <?php echo _("Regulatory Compliance"); ?>
                                    </a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3 mb-4 mb-md-0">
                     <div class="menu-footer-menu-col-2-solution-en-container">
                        <ul id="trustworks-footer-menu-col-2" class="menu">
                           <li id="menu-item-325"
                              class="menu-item menu-item-type-custom menu-item-object-custom menu-item-325">
                              <a href="/methodology" title="Learn about our Methodology">
                                 <?php echo _("METHODOLOGY"); ?>
                              </a>
                           </li>
                           <li id="menu-item-325"
                              class="menu-item menu-item-type-custom menu-item-object-custom menu-item-325">
                              <a href="/partners" title="Explore Our Partners">
                                 <?php echo _("PARTNERS"); ?>
                              </a>
                           </li>
                           <li id="menu-item-325"
                              class="menu-item menu-item-type-custom menu-item-object-custom menu-item-325">
                              <a href="/insights" title="Gain Insights and Knowledge">
                                 <?php echo _("INSIGHTS"); ?>
                              </a>
                           </li>
                           <!-- <li id="menu-item-325" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-325"><a href="/company" title="About Our Company"><?php echo _("COMPANY"); ?></a></li>-->
                           <li id="menu-item-325"
                              class="menu-item menu-item-type-custom menu-item-object-custom menu-item-325">
                              <a href="/contact" title="Contact Us">
                                 <?php echo _("CONTACT"); ?>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-6 col-sm-6 col-md-3 mb-4 mb-md-0">
                     <div class="menu-footer-menu-col-2-solution-en-container">
                        <ul id="trustworks-footer-menu-col-2" class="menu">
                           <li id="menu-item-9921"
                              class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9921">
                              <a class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15158">
                                 <?php echo _("LEGAL"); ?>
                              </a>
                              <ul class="sub-menu">
                                 <li id="menu-item-9923"
                                    class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-9923">
                                    <a href="/privacy-policy" title="Read our Privacy Policy">
                                       <?php echo _("Privacy Policy"); ?>
                                    </a>
                                 </li>
                                 <li id="menu-item-9923"
                                    class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-9923">
                                    <a href="/terms-of-use" title="View Terms of Use">
                                       <?php echo _("Terms of Use"); ?>
                                    </a>
                                 </li>
                                 <li id="menu-item-15158"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15158"><a
                                       href="/legal-notice/" title="Legal Notice Information">
                                       <?php echo _("Legal Notice"); ?>
                                    </a></li>
                                 <li id="menu-item-13055"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13055"><a
                                       href="/cookie-policy" title="Cookie Policy Details">
                                       <?php echo _("Cookie Policy"); ?>
                                    </a></li>
                              </ul>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="copyright d-flex">
            <p class="text-xs">&copy;2021 <b>trustworks<sup>&#x2122;</sup></b>. All Rights Reserved.</p>
         </div>
      </div>
   </footer>

</body>

</html>