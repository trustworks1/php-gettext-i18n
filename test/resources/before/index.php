<?php
// TRANSLATOR required to activate the UTF-8 charset.
_("__required__ø");


?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head></head>

<body class="resource-template-default single single-resource postid-13201  lang-en hrr">
   <!-- End Google Tag Manager (noscript) -->
   <header class="navbar navbar-expand-xl trustworks-navbar">
      <nav class="container-xl" aria-label="Main navigation">
         <a class="navbar-brand" href="/" title="Back to Homepage">
            <img src="assets/images/img_trustworks_logo.svg" alt="trustworks Company Logo">
         </a>
         <button class="navbar-toggler" type="button" aria-label="Toggle navigation">
            <svg class="icon-hamburger" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="#fff"
               viewBox="0 0 16 16">
               <path fill-rule="evenodd"
                  d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z">
               </path>
            </svg>
            <svg class="icon-close" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.63 15.62">
               <polygon fill="#fff"
                  points="15.63 1.41 14.21 0 7.81 6.4 1.41 0 0 1.41 6.4 7.81 0 14.21 1.41 15.63 7.81 9.23 14.21 15.63 15.63 14.21 9.23 7.81 15.63 1.41" />
            </svg>
         </button>
         <div class="collapse navbar-collapse" id="navbar">
            <div class="menu-header-menu-navigation-container">
               <ul id="trustworks-header-menu" class="menu ">
                  <li
                     class="menu-item menu-item-type-custom menu-item-object-custom nav-item menu-item-has-children dropdown">
                     <a href="/services" class="nav-link dropdown-toggle" title="Explore our range of services">
                        <?php echo _("Services"); ?>
                     </a>
                  </li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom nav-item">
                     <a href="/methodology" class="nav-link" title="Learn about Our Methodology">
                        <?php echo _("Methodology"); ?>
                     </a>
                  </li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom nav-item">
                     <a href="/partners" class="nav-link" title="Explore Our Partners">
                        <?php echo _("Partners"); ?>
                     </a>
                  </li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom nav-item">
                     <a href="/insights" class="nav-link" title="Gain Insights and Knowledge">
                        <?php echo _("Insights"); ?>
                     </a>
                  </li>
                  <!-- <li class="menu-item menu-item-type-custom menu-item-object-custom nav-item">
                        <a href="/about" class="nav-link" title="About Us"><?php echo _("About"); ?></a>
                     </li> -->
                  <li class="menu-item menu-item-type-custom menu-item-object-custom nav-item">
                     <a href="/contact" class="nav-link" title="Contact Us">
                        <?php echo _("Contact"); ?>
                     </a>
                  </li>
               </ul>
               <ul class="navbar-nav trustworks-navbar-right flex-row ms-md-auto">
                  <li class="menu-item demos-dropdown-wrapper">
                     <div class="nav-link btn btn-primary product-demos-dropdown"
                        title="Schedule a Meeting with trustworks" data-cal-link="trustworks/contact"
                        data-cal-namespace="contact" data-cal-config='{"layout":"month_view"}'>
                        <?php echo _("Request a meeting"); ?>
                     </div>

                  </li>
                  <li
                     class="menu-item menu-item-type-custom menu-item-object-custom dropdown nav-item languages-dropdown">
                     <a href="javascript:void(0)" class="nav-link dropdown-toggle" title="Select Your Language">
                        <svg width="33" height="18" enable-background="new 0 0 33.2 18.2" viewBox="0 0 33.2 18.2"
                           xmlns="http://www.w3.org/2000/svg">
                           <title>
                              <?php echo _("Languages"); ?>
                           </title>
                           <path d="m32.4 7.4-3.5 3.5-3.5-3.5" fill="none" stroke="currentColor" stroke-width="2" />
                           <path fill="currentColor"
                              d="m9.2.1c-5 0-9 4-9 9s4 9 9 9 9-4 9-9-4-9-9-9zm0 16.6c-4.2 0-7.6-3.4-7.6-7.6s3.4-7.6 7.6-7.6h.7v1.3h-1.4l-.7 2.2h-.7v.7h3.5l.7.7h.7v.6l-.7.7-2.8-.7-2.8 2.1v2.1l1.4 1.4h2.1v1.7l.7 1.7h.7l2.1-2.8v-.7l.7-1.4v-.7h-.7l-.7-1.3.7-.7 1 .7 1.7-.7v-.7h1.3c.1.5.1.9.1 1.4 0 4.2-3.4 7.6-7.6 7.6z" />
                        </svg>
                     </a>
                     <ul class="dropdown-menu depth_0">                        
                        <?php include PUBLIC_PATH.'assets/menu/langs.php';?>
                     </ul>
                  </li>
               </ul>
               <ul class="menu languages-mobile">
                  <li class="megamenu menu-item menu-item-type-custom menu-item-object-custom dropdown nav-item">
                     <a href="javascript:void(0)" class="nav-link dropdown-toggle" title="Open Dropdown Menu">
                        <svg width="33" height="18" viewBox="0 0 33 18" version="1.1" xmlns="http://www.w3.org/2000/svg"
                           xmlns:xlink="http://www.w3.org/1999/xlink">
                           <title>
                              <?php echo _("Languages"); ?>
                           </title>
                           <path fill="currentColor"
                              d="M9,0 C4.03753846,0 0,4.03753846 0,9 C0,13.9624615 4.03753846,18 9,18 C13.9624615,18 18,13.9624615 18,9 C18,4.03753846 13.9624615,0 9,0 Z M9,16.6153846 C4.80115385,16.6153846 1.38461538,13.1988462 1.38461538,9 C1.38461538,4.80115385 4.80115385,1.38461538 9,1.38461538 C9.234,1.38461538 9.46384615,1.39915385 9.69230769,1.41992308 L9.69230769,2.76923077 L8.30769231,2.76923077 L7.61538462,4.84615385 L6.92307692,4.84615385 L6.92307692,5.53846154 L10.3846154,5.53846154 L11.0769231,6.23076923 L11.7692308,6.23076923 L11.7692308,6.92307692 L11.0769231,7.61538462 L8.30769231,6.92307692 L5.53846154,9 L5.53846154,11.0769231 L6.92307692,12.4615385 L9,12.4615385 L9,14.1923077 L9.69230769,15.9230769 L10.3846154,15.9230769 L12.4615385,13.1538462 L12.4615385,12.4615385 L13.1538462,11.0769231 L13.1538462,10.3846154 L12.4615385,10.3846154 L11.7692308,9 L12.4615385,8.30769231 L13.5,9 L15.2307692,8.30769231 L15.2307692,7.61538462 L16.4831538,7.61538462 C16.5662308,8.06538462 16.6153846,8.52646154 16.6153846,9 C16.6153846,13.1988462 13.1988462,16.6153846 9,16.6153846 Z">
                           </path>
                        </svg>
                     </a>
                     <ul class="dropdown-menu depth_0">                        
                        <?php include PUBLIC_PATH.'assets/menu/langs.php';?>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
   </header>
   <section class="block-hero-full-image">
      <div class="container-xl">
         <div class="row">
            <div class="col-12 col-md-8">
               <div class="hero-content">
                  <h1 class="lrg">
                     <?php echo _("Security, Quality and Regulatory"); ?>
                  </h1>
                  <p>
                     <?php echo _("Enhancing Information Security, Elevating Quality Assistance and Ensuring Regulatory Compliance through our Trust Systems Integration Methodology."); ?>
                  </p>
                  <a class="btn btn-primary btn-lg" href="methodology" target="_self"
                     title="Learn more about our methodology">
                     <span>
                        <?php echo _("Discover our Methodology"); ?>
                     </span>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <img decoding="async" class="hero-bg-img" loading="eager" fetchpriority="high"
         src="assets/images/img_homepage_banner.svg" alt="Background gradient" width="2560" height="1200">
   </section>
   <div class="in-page-nav-wrapper">
      <nav class="in-page-nav mb-0">
         <a class="js-in-page-link" href="#test-automation" target="_self" title="Navigate to Test Automation section">
            <?php echo _("Test Automation"); ?>
         </a>
         <a class="js-in-page-link" href="#enterprise-devsecops" target="_self"
            title="Navigate to Enterprise DevSecOps section">
            <?php echo _("Enterprise DevSecOps"); ?>
         </a>
         <a class="js-in-page-link" href="#information-security" target="_self"
            title="Navigate to Information Security section">
            <?php echo _("Information Security"); ?>
         </a>
         <a class="js-in-page-link" href="#quality-assistance" target="_self"
            title="Navigate to Quality Assistance section">
            <?php echo _("Quality Assistance"); ?>
         </a>
         <a class="js-in-page-link" href="#regulatory-compliance" target="_self"
            title="Navigate to Regulatory Compliance section">
            <?php echo _("Regulatory Compliance"); ?>
         </a>
      </nav>
   </div>

</body>
</html>
