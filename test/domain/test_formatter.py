import logging

from src.domain.formatter import Msg_Formatter

logger = logging.getLogger(__name__)

def test_remove_back_slash(msg_formatter: Msg_Formatter):
    sample_text = r"In such as \"regulation\", \"directive\", \"standard\", and \"norm\" carry specific,."
    expected_text = r'In such as "regulation", "directive", "standard", and "norm" carry specific,.'
    result = msg_formatter.remove_quotes_backslash(sample_text)
    logger.debug(f"Result is '{result}'")
    assert expected_text == result
    
    
def test_add_back_slash(msg_formatter: Msg_Formatter):
    sample_text = r'In such as "regulation", "directive", "standard", and "norm" carry specific,.'
    expected_text = r"In such as \"regulation\", \"directive\", \"standard\", and \"norm\" carry specific,."
    result = msg_formatter.add_quotes_backslash(sample_text)
    logger.debug(f"Result is '{result}'")
    assert expected_text == result
    
    
def test_trim_extra_whitespaces(msg_formatter: Msg_Formatter):
    sample_text = "          our service  aims to seamlessly   \n"
    expected_text = " our service aims to seamlessly \n"
    result = msg_formatter.trim_extra_spaces(sample_text)
    #logger.debug(f"Result is '{result}'")
    assert result == expected_text
    

def test_remove_new_line(msg_formatter: Msg_Formatter):
    sample_text = "aims to seamlessly \n"
    expected_text = "aims to seamlessly "
    result = msg_formatter.remove_new_line(sample_text)
    #logger.debug(f"Result is '{result}'")
    assert result == expected_text
    
