import pytest
import logging
import filecmp

from src.domain.translator_service import Translator_Service
from src.infrastructure.translator_EN import Translator_EN

logger = logging.getLogger(__name__)

@pytest.mark.parametrize("sample_text, expected", 
                         [("this is a sentence", True), 
                          ("(SSD) is not a tradfasd", True),
                          (".a", True),
                          (".", False)])
def test_string_is_translatable(
        sample_text, expected, translator_service: Translator_Service):
    assert translator_service.is_translatable(sample_text) == expected
    

def test_translate_empty_msgid(
        empty_msgids, empty_msgid_path, empty_msgid_translated_path, translator_service):
    content = translator_service.translate(empty_msgids, language="es")
    with open(empty_msgid_translated_path, 'w', encoding='utf-8') as f:
        f.write(content)
    assert 'msgid ""' in content
    assert 'msgstr ""' in content
    assert '# Automatically generated, 2024.' in content    
    assert filecmp.cmp(empty_msgid_path, empty_msgid_translated_path, shallow=False)

    
def test_translate_single_msgid(
        monkeypatch, single_msgids, single_line_msgid_translated_path, translator_service):
    _stub_translation_return_value(monkeypatch)
    content = translator_service.translate(single_msgids, language="es")
    # Uncomment for visual regression.
    _write_file(single_line_msgid_translated_path, content)
    assert 'msgstr "translated - Services..."' in content
    assert 'msgstr "translated - In the sph..."' in content
    assert 'msgstr "Contenido traducido al ES ...."' in content



def test_translate_multiline_msgid(
        monkeypatch, multi_line_msgids, multi_line_msgid_translated_path, translator_service):
    _stub_translation_return_value(monkeypatch)
    content = translator_service.translate(multi_line_msgids, language="es")
    # Uncomment for visual regression.
    _write_file(multi_line_msgid_translated_path, content)
    assert "translated - In today's..." in content
    assert "translated - Employing ..." in content
    assert "translated - With a com..." in content
    assert "translated - With over ..." in content
    assert "translated - The value ..." in content
    assert "translated - Expanding ..." not in content
    assert "This is already translated content..." in content
    assert content.count("This is already translated content...") == 1
    

def test_should_log_a_warning_on_missing_msgstr(caplog, monkeypatch, 
        multi_line_msgids_no_msgstr, translator_service):
    _stub_translation_return_value(monkeypatch)   
    with caplog.at_level(logging.WARNING):
        content = translator_service.translate(multi_line_msgids_no_msgstr, language="es")        
        assert "msgstr missing for msgid at line" in caplog.text
    assert "translated - The object..." in content
     

def _stub_translation_return_value(monkeypatch):
    monkeypatch.setattr(Translator_EN, 'translate', lambda *args, **kwargs: f"translated - {args[1][:10]}...")
    
    
def _write_file(file_path, content):
    with open(file_path, 'w', encoding='utf-8') as f:
        f.write(content)
