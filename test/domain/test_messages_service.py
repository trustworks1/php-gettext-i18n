import pytest
from src.domain.messages_service import Messages_service


@pytest.mark.parametrize("sample_text, expected",
                         [('msgid ""', ""),
                          ('msgid "Services"', "Services")])
def test_should_read_msgid_line(sample_text, expected, messages_service: Messages_service):
    result = messages_service.read_msgid_content(sample_text)
    assert result != None
    assert result == expected
    
    
@pytest.mark.parametrize("sample_text, expected",
                         [('msgstr ""', ""),
                          ('msgstr "Servicios"', "Servicios")])
def test_should_read_msgstr_line(sample_text, expected, messages_service: Messages_service):
    result = messages_service.read_msgstr_content(sample_text)
    assert result != None
    assert result == expected
    

@pytest.mark.parametrize("sample_list, expected",
                         [(['msgid "Services"', 'msgstr ""'], False),
                          (['msgid "Services"', 'msgstr "Servicios"'], True)])
def test_is_translated(sample_list, expected, messages_service: Messages_service):
    assert messages_service.is_single_line_translated(sample_list, 0) == expected
    