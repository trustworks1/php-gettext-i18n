import pytest
import sys

from pathlib import Path
from typing import List
from src.infrastructure.translator_EN import Translator_EN
from src.domain.formatter import Msg_Formatter
from src.domain.translator_service import Translator_Service
from src.domain.messages_service import Messages_service

# Append the src directory to sys.path
sys.path.append(str(Path(__file__).parent.parent / "src"))

TRANSL_RESOURCES_PATH = "test/resources"

@pytest.fixture
def temp_file_path(tmp_path):
    return tmp_path / "tempfile.txt"

def read_lines_test_file(file_path: Path):
    with open(file_path, 'r', encoding='utf-8') as f:
        return f.readlines()
    
# Empty base cases of msgids
@pytest.fixture
def empty_msgid_path() -> str:
    return f"{TRANSL_RESOURCES_PATH}/translations/empty_msgid.po"

@pytest.fixture
def empty_msgid_translated_path() -> str:
    return f"{TRANSL_RESOURCES_PATH}/output/empty_msgid-translated.po"

@pytest.fixture
def empty_msgids(empty_msgid_path) -> List[str]:
    return read_lines_test_file(empty_msgid_path)

# Single line cases of msgid
@pytest.fixture
def single_line_msgid_path() -> str:
    return f"{TRANSL_RESOURCES_PATH}/translations/single_line_msgid.po"

@pytest.fixture
def single_line_msgid_translated_path() -> str:
    return f"{TRANSL_RESOURCES_PATH}/output/single_line_msgid-translated.po"

@pytest.fixture
def single_msgids(single_line_msgid_path) -> List[str]:    
    return read_lines_test_file(single_line_msgid_path)

# Multi line cases of msgid
@pytest.fixture
def multi_line_msgid_path() -> str:
    return f"{TRANSL_RESOURCES_PATH}/translations/multi_line_msgid.po"

@pytest.fixture
def multi_line_msgid_translated_path() -> str:
    return f"{TRANSL_RESOURCES_PATH}/output/multi_line_msgid-translated.po"

@pytest.fixture
def multi_line_msgids(multi_line_msgid_path) -> List[str]:    
    return read_lines_test_file(multi_line_msgid_path)

@pytest.fixture
def multi_line_msgids_no_msgstr() -> List[str]:    
    return read_lines_test_file(f"{TRANSL_RESOURCES_PATH}/translations/multi_line_msgid_no_msgstr.po")

@pytest.fixture
def multi_line_msgids_no_msgstr_value() -> List[str]:    
    return read_lines_test_file(f"{TRANSL_RESOURCES_PATH}/translations/multi_line_msgid_no_msgstr_value.po")


# App objects
@pytest.fixture
def translator_service() -> Translator_Service:
    return Translator_Service()

@pytest.fixture
def messages_service() -> Messages_service:
    return Messages_service()

@pytest.fixture
def translator_en_to_es() -> Translator_EN:
    return Translator_EN("es")

@pytest.fixture
def msg_formatter() -> Msg_Formatter:
    return Msg_Formatter()

